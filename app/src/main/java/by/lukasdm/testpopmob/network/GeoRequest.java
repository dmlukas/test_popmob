package by.lukasdm.testpopmob.network;

import android.location.Location;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.Arrays;
import java.util.Locale;

import by.lukasdm.testpopmob.entity.GeoPoint;

public class GeoRequest extends RetrofitSpiceRequest<GeoPoint[], IGeoApi> {

    private final String TERM;
    private final Location CURRENT_LOCATION;


    public GeoRequest(String term, Location loc) {
        super(GeoPoint[].class, IGeoApi.class);
        this.TERM = term;
        this.CURRENT_LOCATION = loc;
    }

    @Override
    public GeoPoint[] loadDataFromNetwork() throws Exception {
        GeoPoint[] array = getService().getSuggestPosition(Locale.getDefault().getCountry(), TERM);
        Location toLoc = new Location("Api");
        if (CURRENT_LOCATION != null) {
            for (GeoPoint p : array) {
                toLoc.setLatitude(p.getGeoPosition().getLatitude());
                toLoc.setLongitude(p.getGeoPosition().getLongitude());
                p.setDistance(CURRENT_LOCATION.distanceTo(toLoc));
            }
            Arrays.sort(array);
        }
        return array;
    }
}
