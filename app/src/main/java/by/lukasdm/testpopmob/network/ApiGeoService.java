package by.lukasdm.testpopmob.network;

import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

import by.lukasdm.testpopmob.BuildConfig;
import retrofit.RestAdapter;

public class ApiGeoService extends RetrofitGsonSpiceService {

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(IGeoApi.class);
    }

    @Override
    protected String getServerUrl() {
        return BuildConfig.API_HOST_NAME;
    }

    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        return super.createRestAdapterBuilder()
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE);
    }
}
