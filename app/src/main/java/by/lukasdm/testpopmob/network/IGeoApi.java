package by.lukasdm.testpopmob.network;

import by.lukasdm.testpopmob.entity.GeoPoint;
import retrofit.http.GET;
import retrofit.http.Path;

public interface IGeoApi {

    @GET("/position/suggest/{locale}/{term}")
    GeoPoint[] getSuggestPosition(@Path("locale") String locale, @Path("term") String term);
}
