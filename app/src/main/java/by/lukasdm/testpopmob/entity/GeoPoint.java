package by.lukasdm.testpopmob.entity;

import com.google.gson.annotations.SerializedName;

public class GeoPoint implements Comparable {

    @SerializedName("_id")
    private long id;
    private String name;
    private String fullName;
    private String type;
    private String country;
    @SerializedName("geo_position")
    private GeoPosition geoPosition;
    private long locationId;
    private boolean inEurope;
    private String countryCode;
    private boolean coreCountry;

    //local fields
    private float mDistance;

    public String getCombName() {
        return name + " (" + country + ")";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public GeoPosition getGeoPosition() {
        return geoPosition;
    }

    public void setGeoPosition(GeoPosition geoPosition) {
        this.geoPosition = geoPosition;
    }

    public long getLocationId() {
        return locationId;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    public boolean isInEurope() {
        return inEurope;
    }

    public void setInEurope(boolean inEurope) {
        this.inEurope = inEurope;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public boolean isCoreCountry() {
        return coreCountry;
    }

    public void setCoreCountry(boolean coreCountry) {
        this.coreCountry = coreCountry;
    }

    public float getDistance() {
        return mDistance;
    }

    public void setDistance(float mDistance) {
        this.mDistance = mDistance;
    }

    @Override
    public int compareTo(Object another) {
        if (!(another instanceof GeoPoint)) {
            return -1;
        }
        GeoPoint itemAnother = (GeoPoint) another;
        if (mDistance < itemAnother.getDistance()) {
            return -1;
        } else if (mDistance > itemAnother.getDistance()) {
            return 1;
        } else {
            return 0;
        }
    }
}
