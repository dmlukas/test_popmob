package by.lukasdm.testpopmob;

import android.app.Application;
import android.util.Log;

import roboguice.util.temp.Ln;

public class AppApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Ln.getConfig().setLoggingLevel(Log.ERROR);
    }
}
