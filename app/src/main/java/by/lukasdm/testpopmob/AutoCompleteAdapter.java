package by.lukasdm.testpopmob;

import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.concurrent.CountDownLatch;

import by.lukasdm.testpopmob.entity.GeoPoint;
import by.lukasdm.testpopmob.network.ApiGeoService;
import by.lukasdm.testpopmob.network.GeoRequest;

public class AutoCompleteAdapter extends ArrayAdapter<String> {

    private static final int START = 1;
    private static final int STOP = 2;
    private static final int ERROR = 3;

    private GeoPoint[] mData = null;
    private Location mCurLoc;

    private Filter mFilter;
    private ISearchListener mImplSearch;
    private final SpiceManager SPICE_MANAGER = new SpiceManager(ApiGeoService.class);

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            if (mImplSearch == null) {
                return false;
            }

            switch (msg.what) {
                case START:
                    mImplSearch.startSearch();
                    return true;
                case STOP:
                    mImplSearch.stopSearch();
                    return true;

                case ERROR:
                    mImplSearch.errorSearch();
                    return true;
            }
            return false;
        }
    });

    public interface ISearchListener {
        void startSearch();

        void stopSearch();

        void errorSearch();
    }

    public void onStart() {
        if (!SPICE_MANAGER.isStarted()) {
            SPICE_MANAGER.start(getContext());
        }
    }

    public void onStop() {
        SPICE_MANAGER.shouldStop();
    }

    public void addSearchListener(ISearchListener listener) {
        mImplSearch = listener;
    }

    public AutoCompleteAdapter(Context context) {
        super(context, android.R.layout.simple_dropdown_item_1line);
    }

    public void setCurrentLocation(Location loc) {
        this.mCurLoc = loc;
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.length;
    }

    @Override
    public String getItem(int position) {
        return mData[position].getCombName();
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new GeoFilter();
        }
        return mFilter;
    }

    private class GeoFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults filterResults = new FilterResults();

            if (constraint != null && constraint.length() > 1) {
                mHandler.sendEmptyMessage(START);
                final CountDownLatch latch = new CountDownLatch(1);
                GeoRequest request = new GeoRequest(constraint.toString(), mCurLoc);
                SPICE_MANAGER.execute(request, constraint.toString(), DurationInMillis.ONE_MINUTE, new RequestListener<GeoPoint[]>() {
                    @Override
                    public void onRequestFailure(SpiceException spiceException) {
                        mData = null;
                        mHandler.sendEmptyMessage(ERROR);
                        latch.countDown();
                    }

                    @Override
                    public void onRequestSuccess(GeoPoint[] array) {
                        mData = array;
                        mHandler.sendEmptyMessage(STOP);
                        latch.countDown();
                    }
                });
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                filterResults.count = mData.length;
            }
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null && results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
