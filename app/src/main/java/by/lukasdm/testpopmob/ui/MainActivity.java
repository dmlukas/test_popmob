package by.lukasdm.testpopmob.ui;

import android.app.DatePickerDialog;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import by.lukasdm.testpopmob.AutoCompleteAdapter;
import by.lukasdm.testpopmob.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        DatePickerDialog.OnDateSetListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String TAG = "MainActivity";

    private DatePickerDialog mDateDialog;
    private TextView tvLocDetect;
    private EditText edtDate;
    private AutoCompleteTextView acFrom;
    private AutoCompleteTextView acTo;
    private SimpleDateFormat mSdf;
    private InputMethodManager mImm;
    private Button btnSearch;
    private ProgressBar pbFrom;
    private ProgressBar pbTo;

    private AutoCompleteAdapter mAdapterFrom;
    private AutoCompleteAdapter mAdapterTo;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocRequest;
    private boolean isRequestingLocation;

    private GoogleApiClient buildGoogleApiClient() {
        return new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private LocationRequest createLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
        return locationRequest;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvLocDetect = (TextView) findViewById(R.id.tv_loc);
        tvLocDetect.setAnimation(AnimationUtils.loadAnimation(this, R.anim.location_detecting));

        mGoogleApiClient = buildGoogleApiClient();
        mLocRequest = createLocationRequest();

        mImm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        mSdf = new SimpleDateFormat("d.MM.y", Locale.getDefault());

        Calendar c = Calendar.getInstance();
        mDateDialog = new DatePickerDialog(this, R.style.DatePickerTheme, this, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

        pbFrom = (ProgressBar) findViewById(R.id.pb_from);
        pbTo = (ProgressBar) findViewById(R.id.pb_to);

        btnSearch = (Button) findViewById(R.id.btn_search);
        btnSearch.setOnClickListener(this);

        edtDate = (EditText) findViewById(R.id.edt_date);
        edtDate.addTextChangedListener(mWatcher);
        findViewById(R.id.iv_date).setOnClickListener(this);

        acFrom = (AutoCompleteTextView) findViewById(R.id.ac_from);
        acFrom.addTextChangedListener(mWatcher);
        mAdapterFrom = new AutoCompleteAdapter(this);
        mAdapterFrom.addSearchListener(mSearchFrom);
        acFrom.setAdapter(mAdapterFrom);

        acTo = (AutoCompleteTextView) findViewById(R.id.ac_to);
        acTo.addTextChangedListener(mWatcher);
        mAdapterTo = new AutoCompleteAdapter(this);
        mAdapterTo.addSearchListener(mSearchTo);
        acTo.setAdapter(mAdapterTo);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAdapterFrom.onStart();
        mAdapterTo.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAdapterTo.onStop();
        mAdapterFrom.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onPause() {
        stopLocationUpdates();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    private AutoCompleteAdapter.ISearchListener mSearchFrom = new AutoCompleteAdapter.ISearchListener() {
        @Override
        public void startSearch() {
            pbFrom.setVisibility(View.VISIBLE);
        }

        @Override
        public void stopSearch() {
            pbFrom.setVisibility(View.GONE);
        }

        @Override
        public void errorSearch() {
            pbFrom.setVisibility(View.GONE);
            acFrom.setError(getString(R.string.search_error));
        }
    };

    private AutoCompleteAdapter.ISearchListener mSearchTo = new AutoCompleteAdapter.ISearchListener() {
        @Override
        public void startSearch() {
            pbTo.setVisibility(View.VISIBLE);
        }

        @Override
        public void stopSearch() {
            pbTo.setVisibility(View.GONE);
        }

        @Override
        public void errorSearch() {
            pbTo.setVisibility(View.GONE);
            acTo.setError(getString(R.string.search_error));
        }
    };

    private TextWatcher mWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            checkAndEnabledSearch();
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_date:
                mImm.hideSoftInputFromWindow(edtDate.getWindowToken(), 0);
                mDateDialog.show();
                break;

            case R.id.btn_search:
                Toast.makeText(this, R.string.not_yet_impl, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        edtDate.setText(mSdf.format(calendar.getTime()));
        edtDate.requestFocus();
        edtDate.setSelection(edtDate.length());
    }

    private void checkAndEnabledSearch() {
        boolean isEnabled = acTo.getText().length() > 1
                && acFrom.getText().length() > 1
                && edtDate.getText().length() > 8;
        btnSearch.setEnabled(isEnabled);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "onConnected");
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private void startLocationUpdates() {
        if (!isRequestingLocation) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocRequest, this);
            isRequestingLocation = true;
        }
    }

    private void stopLocationUpdates() {
        if (isRequestingLocation) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
            isRequestingLocation = false;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, location.toString());
        mAdapterFrom.setCurrentLocation(location);
        mAdapterTo.setCurrentLocation(location);
        stopLocationUpdates();
        mGoogleApiClient.disconnect();
        tvLocDetect.clearAnimation();
        tvLocDetect.setText(R.string.loc_detected);
    }
}
